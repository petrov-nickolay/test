<?php
	require_once( 'test-class.php' );
?>
<html>
<body>
	<div class="entry-content">
		<h3>Result page</h3>
		<?php
		$nonce_H = new WP_Nonces_Holder( 'wp-nonces-action' );
		if ($nonce_H->security_check()) {
			echo 'Security: pass<br>Form: processing....';
		}
		else {
			echo 'Security: suspended';
		}
		unset($nonce_H);
		?>
	</div><!-- .entry-content -->
</body>
</html>