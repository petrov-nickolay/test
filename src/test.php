<?php
	require_once( 'test-class.php' );
?>
<html>
<body>
	<div class="entry-content">
		<?php
		$nonce_H = new WP_Nonces_Holder( 'wp-nonces-action' );
		$result_url = 'test-result.php';
		?>
		<!-- Non security link -->
		<p><a href = <?php echo $result_url; ?>>Go to Result</a></p>
		
		<!-- Security link -->
		<p><a href = <?php echo $nonce_H->get_nonce_url($result_url); ?>>Go to Result with nonces</a></p>
		
		<!-- Security form -->
		<form id="form" action="<?php echo $result_url; ?>">
			<fieldset>
			<legend>Action Form</legend>
			<?php $nonce_H->get_nonce_field();?>
			<label for="user-name">User Name</label><br>
			<input type="text" name="user-name" class="text" id="user-name"><br>
			<label for="note">Note</label><br>
			<input type="text" name="note" class="text" id="note"><br>
			<label for="action">Action</label><br>
			<select id="action" name="action">
				<option value="edit">Edit</option>
				<option value="suspend">Suspend</option>
				<option value="delete">Delete</option>
			</select><br>
			<input type="submit" class="button" value="Apply">
			</fieldset>
		</form><!-- #form -->
		
		<?php unset($nonce_H); ?>
	</div><!-- .entry-content -->
</body>
</html>