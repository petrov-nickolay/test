<?php
// Load the WordPress library.
require_once( dirname( __FILE__ ) . '/wp-load.php' );

/**
 * Description: OOP implementation of WordPress Nonces
 * Version: 1.0.0
 * Author: Nickolay Petrov
 */
if (! class_exists ( 'WP_Nonces_Holder' )) {
	class WP_Nonces_Holder {
		
		/**
		 *
		 * @var string
		 */
		private $nonce_name;
		
		/**
		 *
		 * @var string
		 */
		private $nonce_action;
				
		/**
		 * Protected constructor to prevent creating a new instance of the
		 * *Singleton* via the `new` operator from outside of this class.
		 */
		function __construct($action = -1, $name = '_wpnonce') {
			self::set_nonce_action($action);
			self::set_nonce_name($name);
		}
		
		/**
		 * Set nonce action
		 *
		 * @param int|string $action    Optional. Nonce action name. Default -1.
		 */
		function set_nonce_action($action = -1) {
			$this->nonce_action = $action;
		}
		
		/**
		 * Set nonce name
		 *
		 * @param string     $name      Optional. Nonce name. Default '_wpnonce'.
		 */
		function set_nonce_name($name = '_wpnonce') {
			$this->nonce_name = $name;
		}
		
		/**
		 * Retrieve URL with nonce added to URL query.
		 *
		 * @param string     $actionurl URL to add nonce action.
		 * @return string Escaped URL with nonce action added.
		 */
		function get_nonce_url( $action_url ) {
			return wp_nonce_url($action_url, $this->nonce_action, $this->nonce_name);
		}
		
		/**
		 *
		 * Retrieve or display nonce hidden field for forms.
		 */
		function get_nonce_field() {
			return wp_nonce_field($this->nonce_action, $this->nonce_name);
		}
		
		/**
		 * Verify that correct nonce was used with time limit.
		 * 
		 * @return false|int False if the nonce is invalid, 1 if the nonce is valid and generated between
		 *                   0-12 hours ago, 2 if the nonce is valid and generated between 12-24 hours ago.
		 */
		 function security_check() {
		 	return wp_verify_nonce($_REQUEST[$this->nonce_name], $this->nonce_action);
//			return check_admin_referer($this->nonce_action, $this->nonce_name);
		}
	}
}
?>