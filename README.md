### The task ###

> Create a composer package, which serves the functionality working with WordPress Nonces. That means to have at least wp_nonce_*() function implemented in an object orientated way. You don't have to replace the current functionality, just implement the WordPress Nonces more like OOP. README and UnitTests are mandatory.

### My solution ###

1. I did a php class in the 'test-class.php' file, which implements WordPress Nonces in an object-oriented way.
2. I created the two files "test.php" and "test-result.php" for the presentation purposes.
3. I made a composer package. Unfortunately this is new for me and slowed me down too much.
4. The composer.json file is located after the README file.
5. Install it in any empty directory.
6. Copy the three files from the directory \vendor\petrov-nickolay\test to any working wordpres installation ('ABSPATH').
7. You can test my work by writing in a browser address line 'http:/yoursite.local/test.php' if you use 'Local by Flywheel' for example.
8. I'm not familiar with UnitTest and probably time is out. If you could explain me what are you using for UnitTest, I will finish this part of the task.

Best regards

Nickolay Petrov